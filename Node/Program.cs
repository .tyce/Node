﻿using System.Runtime.InteropServices;
using Node;

[assembly: CLSCompliant(true)]
[assembly: ComVisible(true)]

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSingleton<IRest, Rest>();

builder.Services.AddEndpointsApiExplorer().AddSwaggerGen();

using var application = builder.Build();

application.MapGet("/", (IRest api) => api.HelloWorld);

application.UseSwagger().UseSwaggerUI();

application.Run();
