﻿using System.Runtime.CompilerServices;
using BenchmarkDotNet.Columns;
using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Exporters.Xml;
using BenchmarkDotNet.Loggers;
using BenchmarkDotNet.Running;

namespace Benchmark;

public static partial class Program
{
	public static void Main(string[] _)
	{
		var rootPath = Path.Combine(new FileInfo(FilePath()).Directory.FullName, "..");

		BenchmarkSwitcher
			.FromAssembly(typeof(Program).Assembly)
			.Run(
				new[] { "--filter=Benchmark.*" },
				ManualConfig
					.CreateEmpty()
					.AddColumnProvider(DefaultColumnProviders.Instance)
					.WithArtifactsPath(Path.Combine(rootPath, ".temp"))
					.WithOption(ConfigOptions.DisableLogFile, true)
					.WithOption(ConfigOptions.JoinSummary, true)
					.AddExporter(HtmlExporter.Default)
					.AddExporter(MarkdownExporter.Default)
					.AddExporter(XmlExporter.Full)
					.AddLogger(ConsoleLogger.Default)
			);

		var resultsDir = Path.Combine(rootPath, ".temp", "results");

		foreach (var file in new DirectoryInfo(resultsDir).GetFiles())
			file.MoveTo(Path.Combine(rootPath, ".info", $"Benchmark{file.Extension}"), true);

		Directory.Delete(resultsDir, true);
	}

	private static string FilePath([CallerFilePath] string path = default) => path;
}
