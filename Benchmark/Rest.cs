﻿using BenchmarkDotNet.Attributes;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using BenchmarkDotNet.Diagnosers;
using Main = Program;

namespace Benchmark
{
	[MemoryDiagnoser]
	[ThreadingDiagnoser]
	public class Rest
	{
		private readonly HttpClient Client;

		public Rest()
		{
			var application = new WebApplicationFactory<Main>().WithWebHostBuilder(builder =>
			{
				builder.UseSetting("http_port", "5000");
				builder.UseEnvironment("Release");
			});

			Client = application.CreateClient();
		}

		[Benchmark(Description = "/")]
		public async Task<HttpResponseMessage> Root() => await Client.GetAsync("/");

		[Benchmark(Description = "/Swagger")]
		public async Task<HttpResponseMessage> Swagger() => await Client.GetAsync("/Swagger");
	}
}
