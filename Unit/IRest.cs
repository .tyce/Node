﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using Node;

namespace Unit;

[TestClass]
public class IRest
{
	[TestMethod]
	public void HelloWorld()
	{
		Node.IRest subject = new Rest();

		Assert.AreEqual("Hello World", subject.HelloWorld);
	}
}
