//////////////////////////////////////////////////////////////////////
// ARGUMENTS
//////////////////////////////////////////////////////////////////////

var configuration = Argument("configuration", "Release");
var target = Argument("target", "Publish");

//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////


Task("Format")
	.Does(() =>
	{
		DotNetFormat(
			"./Node.sln",
			new DotNetFormatSettings
			{
				Report = "./.info/format.json",
				Verbosity = DotNetVerbosity.Normal,
				ArgumentCustomization = args => args.Append("--severity info")
			}
		);

		DotNetTool(".", "dotnet-csharpier", ".");
	});

Task("Clean")
	.Does(
		() => DotNetClean("./Node.sln", new DotNetCleanSettings { Configuration = configuration, })
	);

Task("Restore").Does(() => DotNetRestore("./Node.sln"));

Task("Build")
	.IsDependentOn("Format")
	.IsDependentOn("Clean")
	.IsDependentOn("Restore")
	.Does(() =>
	{
		DotNetBuild(
			"./Node.sln",
			new DotNetBuildSettings { Configuration = configuration, NoRestore = true, }
		);
	});

Task("Unit")
	.IsDependentOn("Build")
	.Does(() =>
	{
		DotNetTest(
			"./Unit/Unit.csproj",
			new DotNetTestSettings
			{
				Configuration = configuration,
				NoBuild = true,
				ArgumentCustomization = args =>
					args.Append("/p:CollectCoverage=true")
						.Append("/p:CoverletOutputFormat=\\\"lcov,opencover,cobertura\\\"")
						.Append("/p:CoverletOutput=../.info/unit")
						.Append("/p:ExcludeByFile=\"**/Node/Program.cs\"")
						.Append("--logger")
						.AppendQuoted("trx;logfilename=../.info/unit.log.trx")
						.Append("--results-directory")
						.AppendQuoted("./.temp")
						.Append("--no-build")
						.Append("/p:Threshold=\"90\"")
			}
		);
	});

Task("Integration")
	.IsDependentOn("Build")
	.Does(() =>
	{
		var environment = configuration == "Release" ? "Production" : "Development";

		DotNetTest(
			"./Integration/Integration.csproj",
			new DotNetTestSettings
			{
				Configuration = configuration,
				NoBuild = true,
				ArgumentCustomization = args =>
					args.Append("/p:CollectCoverage=true")
						.Append("/p:CoverletOutputFormat=\\\"lcov,opencover,cobertura\\\"")
						.Append("/p:CoverletOutput=../.info/integration")
						.Append("/p:Threshold=\"90\"")
						.Append("--logger")
						.AppendQuoted($"trx;logfilename=./../.info/integration.log.trx")
						.Append("--results-directory")
						.AppendQuoted("./.temp")
						.Append("--no-build")
						.Append(
							$"-- TestRunParameters.Parameter(name=\\\"Environment\\\",value=\\\"{environment}\\\")"
						)
			}
		);
	});

Task("Test").IsDependentOn("Unit").IsDependentOn("Integration");

Task("Benchmark")
	.IsDependentOn("Build")
	.Does(() =>
	{
		if (configuration != "Release")
			return;

		DotNetRun(
			"./Benchmark/Benchmark.csproj",
			new DotNetRunSettings { Configuration = "Release", NoBuild = true }
		);
	});

Task("Analyze")
	.IsDependentOn("Build")
	.Does(() =>
	{
		try
		{
			DotNetTool(
				"./Node.sln",
				"gendarme",
				$"--config .gendarme.rules.xml --log ./.info/analysis.log --html ./.info/analysis.html --xml ./.info/analysis.xml {Directory("./Node/bin") + Directory(configuration)}/net6.0/Node.dll"
			);
		}
		catch (CakeException error) when (error.ExitCode == 1)
		{
			Spectre.Console.AnsiConsole.MarkupLine("[bold orange1]^^^^^ defects found ^^^^^[/]");
		}
	});

Task("Publish")
	.IsDependentOn("Analyze")
	.IsDependentOn("Test")
	.IsDependentOn("Benchmark")
	.Does(() =>
	{
		DotNetPublish(
			"./Node/Node.csproj",
			new DotNetPublishSettings { Configuration = configuration, NoBuild = true }
		);
	});

//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////

RunTarget(target);
