﻿using System.Net.Http;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Integration;

[TestClass]
public class Rest
{
	public static string Environment { get; set; } = "Development";

	[ClassInitialize]
	public static void Initialize(TestContext context)
	{
		Environment = context.Properties["Environment"]?.ToString() ?? Environment;
	}

	[TestMethod]
	public async Task TestMethod3()
	{
		var response = await Request("/");

		var content = await response.Content.ReadAsStringAsync();

		response.EnsureSuccessStatusCode();
		Assert.AreEqual("utf-8", response.Content.Headers.ContentType?.CharSet);
		Assert.AreEqual("text/plain", response.Content.Headers.ContentType?.MediaType);
		Assert.AreEqual("Hello World", content);
	}

	[TestMethod]
	public async Task TestMethod2()
	{
		var response = await Request("/Swagger");
		response.EnsureSuccessStatusCode();

		Assert.AreEqual("utf-8", response.Content.Headers.ContentType?.CharSet);
		Assert.AreEqual("text/html", response.Content.Headers.ContentType?.MediaType);
	}

	private static async Task<HttpResponseMessage> Request(string endPoint)
	{
		var application = new WebApplicationFactory<Program>().WithWebHostBuilder(builder =>
		{
			builder.UseSetting("http_port", "5000");
			builder.UseEnvironment(Environment);
		});

		var client = application.CreateClient();
		return await client.GetAsync(endPoint);
	}
}
